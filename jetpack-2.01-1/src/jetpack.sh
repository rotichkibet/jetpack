#!/bin/bash
#permDate=$(date +%s --date "2019-1-01")
currentDate=$(date +%s)

conFile="./.config.sh"

echo $currentDate >> $conFile

firstExec=`head -1 $conFile`

lastExec=`tail -1 $conFile`

diff=$(($lastExec-$firstExec))
#set 2592000 30 day trial

if [[ ! $diff -gt 31104000  ]]; then 
defaultDir="$(pwd)"

parentdir=${defaultDir##*/}

#echo $parentdir 

read -p "Enter project name (${parentdir} ?): "  prName

if [[ ${#prName[0]} == 0 ]]; then
    prName=$parentdir
    mkdir -p ./${prName}/{public,templates,variables,scripts}
    read -p "Enter application entry point (default 'app'?): "  appName
        appName=${appName//[[:blank:]]/}
    if [[ ${#appName[0]} == 0 ]]; then
        appName=app
    elif [[ ! ${#appName[0]} == 0 ]]; then
        appName="${appName%.*}"
    fi
    #sudo mkdir -p ~/.config/systemd/user
elif [[ ! ${#prName[0]} == 0 ]]; then
    prName=${prName//[[:blank:]]/}
    mkdir -p ./$prName/{public,templates,variables,scripts}
    read -p "Enter application entry point (default 'app'?)  : "  appName
        appName=${appName//[[:blank:]]/}
    if [[ ${#appName[0]} == 0 ]]; then
        appName=app
 
    elif [[ ! ${#appName[0]} == 0 ]]; then
        appName="${appName%.*}"
 
    fi
    #sudo mkdir -p ~/.config/systemd/user
fi
echo "============ wait as we prepare your bang project ================="
#sleep 2s
# Writing files
#======================= begin navbar =============================0
navgensc=./$prName/scripts/navgen.sh
(
cat << 'nav_items'
#!/bin/bash

filepath="./templates/nav.sh"
routepath=`ls -1 ./public/*.html 2>/dev/null | wc -l`
# begin main loop
if [[ ! -f $filepath ]] && [[ ! $routepath != 0 ]]; then
  echo "Navbar list items will be created at $filepath ..."
  read -p "Enter navbar items (#word.html separated by spaces): "  navbar
  navbar="api-fetch"" ""${navbar[@]}"
  echo "#!/bin/bash" >> ./templates/nav.sh
  echo "read -r -d '' nav0 <<- EOM " >> ./templates/nav.sh
  navig=("")
  for route in ${navbar[@]}
  do
    basenames=${route%.*} # extract the file basename
    basenames=${basenames[@]^^} # convert to uppercase
    case "${basenames[@]}" in  *"INDEX"*) echo -e "\e[93mWarning:'Index' is a reserved route. It was excluded!\e[0m" ;; esac
    basenames="${basenames[@]/INDEX/}" # exclude index route
    echo "<li class=\"nav-item\"><a class=\"nav-link\" id=\"nav-links\" href=\"./${basenames[@],,}.html\">${basenames[@]}</a></li>" >> ./templates/nav.sh
    navig=( "${navig[@]}" "${basenames[@],,}" )
  done
  echo "EOM" >> ./templates/nav.sh
  chmod +x ./templates/nav.sh
  for i in ${navig[@]}
  do
  echo "${i[@]}" " list item..... created in ./templates/nav.sh"
  done
  echo "Ready for development? Start by executing 'npm run rot' to generate routes now ..."
elif [[ ! -f $filepath ]] && [[ $routepath != 0 ]]; then
  echo "Existing routes will be used to build navigation bar"
  read -p "Want to continue? (yes/no): " proceed
  if [[ $proceed == "yes" ]] || [[ $proceed == "y" ]]; then
    yourfilenames=`ls ./public/*.html` 2>/dev/null
    routeList=""
    for eachfile in $yourfilenames
    do
      PATH=${eachfile%.*}
      fileName="${PATH##*/}"
      routeList=( "${routeList[@]}" "${fileName[@]}" $'\n' )
    done
    navbar=${routeList[@]}
    echo "#!/bin/bash" >> ./templates/nav.sh
    echo "read -r -d '' nav0 <<- EOM " >> ./templates/nav.sh
    navig=("")
    for route in ${routeList[@]}
    do
      basenames=${route[@],,} # convert to lowercase
      case "${basenames[@]}" in  *"index"*) echo -e "\e[93mWarning:'Index' is a reserved route. It was excluded!\e[0m" ;; esac
      basenames=${basenames[@]/index/} # remove index reserved route
      basenames=${basenames[@]/login/} # no need to create login, register, and recover links
      basenames=${basenames[@]/register/}
      basenames=${basenames[@]/recover/}
      echo "<li class=\"nav-item\"><a class=\"nav-link\" id=\"nav-links\" href=\"./${basenames[@]}.html\">${basenames[@]^}</a></li>" >> ./templates/nav.sh
      navig=( "${navig[@]}" "${basenames[@]}" )
      rm ./public/.html 2>/dev/null
    done
    echo "EOM" >> ./templates/nav.sh
    chmod +x ./templates/nav.sh 2>/dev/null
    for i in ${navig[@]}
    do
      echo "${i[@]}" " list item..... created in ./templates/nav.sh"
    done
    echo "Ready for development? execute 'npm run rot' now ..."
  elif [[ $proceed == "no" ]] || [[ $proceed == "n" ]]; then
    echo -e "\e[93mWarning! She-Bang discourages manual creation of Navbar list items!\e[0m"&&exit
  else
    echo -e "\e[31mError! Incorrect entry. Try running the script again with correct syntax\e[0m"&&exit
  fi
elif [[ -f $filepath ]] && [[ $routepath != 0 ]]; then
  echo "Navbar items and routes already exists..."
  read -p "Warning: Sure you want to overide existing? routes will be removed (yes/no) : "  remove
  if [[ $remove == "yes" ]] || [[ $remove == "y" ]]; then
    echo "... cleaning the directory"
    rm ./templates/nav.sh
    rm ./public/*.html
    read -p "Enter navbar items (#word.html separated by spaces): "  navbar
    echo "#!/bin/bash" >> ./templates/nav.sh
    echo "read -r -d '' nav0 <<- EOM " >> ./templates/nav.sh
    navig=("")
    for route in ${navbar[@]}
    do
      basenames=${route%.*} # extract the file basename
      basenames=${basenames[@],,} # convert to lowercase
      case "${basenames[@]}" in  *"index"*) echo -e "\e[93mWarning:'Index' is a reserved route. It was excluded!\e[0m" ;; esac
      basenames="${basenames[@]/index/}" # exclude index route
      echo "<li class=\"nav-item\"><a class=\"nav-link\" id=\"nav-links\" href=\"./${basenames[@]}.html\">${basenames[@]^^}</a></li>" >> ./templates/nav.sh
      navig=( "${navig[@]}" "${basenames[@]}" )
    done
    echo "EOM" >> ./templates/nav.sh
    chmod +x ./templates/nav.sh
    echo "${navig[@]}" " list items..... created in ./templates/nav.sh"
  elif [[ $remove == "no" ]] || [[ $remove == "n" ]]; then
    echo $'Existing build will be retained.\nRun the command *npm run install* in your project root directory.\nGenerate routes by executing *npm run routgen*'
  else
  echo -e "\e[31mError! Incorrect entry. Try running the script again with correct syntax\e[0m"&&exit
  fi
elif [[ -f $filepath ]] && [[ ! $routepath != 0 ]]; then
  echo $'\e[32mAll set! proceed to route generation.\nExecute *npm run routgen* in your project root directory\e[32m'
else
  echo -e "\e[31mError! Incorrect entry. Try running the script again with correct syntax\e[0m"&&exit
fi
# end main loop
exit_status=$?
if [ $exit_status -ge 1 ]; then
  echo -e "\e[31mError! Script exited with errors. Please check individual error messages to fix this\e[31m"&&exit
else
  echo "Navigation bar list items are stored in /templates/nav.sh"
fi
exit $exit_status

nav_items
) > $navgensc

#====================== end navbar =============================================0

#======================= litem gen =============================================0
litem_read=./$prName/scripts/litem-read.sh
(
cat << 'litem_gens'
#!/bin/bash

RED='\033[0;31m' # change error message to red
NC='\033[0m' # No Color 

input="./templates/nav.sh" 2>/dev/null
lite=("")
while IFS='' read -r line || [[ -n "$line" ]]
  do
  IFS= read -r arr<<< "$line"
  IFS='=' read -r class className linkType  f <<< "$arr"
  IFS='>' read -r items route <<< "$f"
  IFS='/' read -r del fileExt <<< "$items"
  IFS='"' read -r first secnd <<< "$fileExt"
  lite=("${lite[@]}" "${first[@]}") 
  #echo "${first[@]}"
  done < "$input" 2>/dev/null

 item=${lite[@]%.*} # extract the file basename
 #echo ${item[@]%.*}
 basenames=${item[@],,} # convert to lowercase
 case "${basenames[@]}" in  *"index"*) echo -e "${RED}Error! 'Index' is a reserved route. It was excluded!${NC}" ;; esac
 routes=${basenames[@]/index/}
 
 lit1=${lite[@]/recover.html/}
 lit2=${lit1[@]/login.html/}
 lit3=${lit2[@]/register.html/}
 litem=${lit3[@]/index.html/}
 #echo ${litem[@]}

litem_gens
) > $litem_read
#======================= end litem =============================================0



#====================== headtags ===============================================0
head_tags=./$prName/templates/headtags.sh
(
cat << 'head_tags'
#!/bin/bash

read -r -d '' headtags <<- EOM
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--<meta http-equiv="refresh" content="5" >-->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <meta http-equiv="X-UA-Compatible" content="ie=edge" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <link rel="shortcut icon" href="#" />
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel = "stylesheet" type = "text/css" href = "./shared/main.css" />    
      <link
      rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"/>
EOM

head_tags
) > $head_tags

#======================= end end headtags =====================================0

#====================== footer ================================================0
foot_tags=./$prName/templates/footer.sh
(
cat << 'footer_tags'
#!/bin/bash

read -r -d '' footer <<- EOM
<footer class="page-footer font-small special-color-dark pt-4">

<div class="container">
   <!-- Social buttons -->
    <ul class="list-unstyled list-inline text-center">
      <li class="list-inline-item">
        <a class="btn-floating btn-fb mx-1" href="https://www.facebook.com/nicolus.rottie.9">
          <i class="fab fa-facebook-f"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a class="btn-floating btn-tw mx-1" href="https://twitter.com/TheRotich">
          <i class="fab fa-twitter"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a class="btn-floating btn-gplus mx-1" href="https://scholar.google.fi/citations?user=IJRM18YAAAAJ&hl=en">
          <i class="fab fa-google-plus-g"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a class="btn-floating btn-li mx-1" href="https://linkedin.com/in/rotichtheengineer/">
          <i class="fab fa-linkedin-in"> </i>
        </a>
      </li>
    </ul>
    <!-- Social buttons -->

  </div>

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3"> Copyright &copy; <script>document.write(new Date().getFullYear())</script>:
    <a href="https://jet-pack.netlify.com/"> https://jet-pack.netlify.com/ </a>
  </div>
  <!-- Copyright -->

</footer>
EOM
footer_tags
) > $foot_tags

#======================= end footer =====================================0

#======================== route generator ===============================0

router=./$prName/scripts/gen.sh
(
cat << 'routing'
#!/bin/bash

source ./scripts/home.sh
source ./scripts/litem-read.sh
source ./templates/login.sh
source ./templates/regist.sh 
source ./templates/recover.sh

# Reading the source file for navitation bar list items
# Case 1: if nav.sh exists
readPath="./templates/nav.sh" 2>/dev/null
routepath=`ls -1 ./public/*.html 2>/dev/null | wc -l`

if [[ -f $readPath ]] && [[ ! $routepath != 0 ]]; then
 echo "Navbar list items were detected. She-bang will create the routes from this list"
 item=${litem[@]%.*} # extract the file basename
 item=${litem[@]%.*} # extract the file basename 
 basenames=${item[@],,} # convert to lowercase
 case "${basenames[@]}" in  *"index"*) echo -e "\e[93mWarning: 'Index' is a reserved route. It was excluded!\e[0m" ;; esac
 routes=${basenames[@]/index/} # exclude index reserved rout for the server
  
# Case 2: if nav.sh doesn't exist but route files exist
elif [[ ! -f $readPath ]] && [[ $routepath != 0 ]]; then
echo "No navbar list items were detected. She-bang will create the list from existing routes"
echo "-------------------------------------------------------------"
echo "Menu item lists"
echo "-------------------------------------------------------------"
yourfilenames=`ls ./public/*.html` 2>/dev/null
  navig=("")
  for eachfile in $yourfilenames
    do
    PATH=${eachfile%.*}
    nameVar="${PATH##*/}"
    navig=( "${navig[@]}" "${nameVar[@]}.html" )

    done
  case "${navig[@]}" in  *"index.html"*) echo -e "\e[93mWarning: 'Index' is a reserved route. It was excluded!\e[0m" ;; esac       
  routes=${navig[@]/index.html/} # remove index reserved route
  routes=${routes[@]/login.html/} # no need to create login, register, and recover links
  routes=${routes[@]/register.html/} 
  routes=${routes[@]/recover.html/}
  echo "#!/bin/bash" >> ./templates/nav.sh
  echo "read -r -d '' nav0 <<- EOM " >> ./templates/nav.sh
  for route in ${routes[@]}
  do
  item="${route%.*}" # extract the file basename
  basenames=${item[@],,} # convert to lowercase
  basenames="${basenames[@]/index/}" # exclude index route
  echo "<li class=\"navigation\"><a class=\"nav-link\" id=\"nav-links\" href=\"./${basenames[@]}.html\">${basenames[@]^^}</a></li>" >> ./templates/nav.sh
  echo "${basenames[@]^}" " menu list item..... created in ./templates/nav.sh"
  done
echo "EOM" >> ./templates/nav.sh

/bin/chmod +x ./templates/nav.sh

else 
  #echo "Route files generated successfully"
  echo -e "\e[32mRoute files generated successfully ...\e[0m"
  #echo -e "\e[31mHello World\e[0m"
fi
# Return the corresponding file names in the public directory to be read out
# Tracing the file paths recursively
input="./templates/nav.sh" 2>/dev/null
liText=("")
routeN=("")

filePath=("")
while IFS='' read -r line || [[ -n "$line" ]]
do
  IFS= read -r arr<<< "$line"
  IFS='.h' read -r first second <<< "$arr"
  IFS='>' read -r file rout <<< "$second"
  IFS='</' read -r one two three <<< "$rout"
  IFS='/' read -r section1 section2 <<< "$file"
  liText=("${liText[@]}" "${section2[@]%.*}") 
  routeN=("${routeN[@]^}" "${one[@]}") 
done < "$input" 2>/dev/null

nav0=("")
filePath=( )
for i in ${liText[@]}
do 
  nav0=("${nav0[@]}" "<li class=\"nav-item\"><a class=\"nav-link\" id=\"nav-links\" href=\"./${i[@]}.html\">${i[@]^^}</a></li>" $'\n')
  new="./public/${i[@]}.html"
  filePath=(${filePath[@]} "${new[@]}")
done
# Remove "./.html" character resulting from index route removal

while read -r line
do
  [[ ! $line =~ "./.html" ]] && echo "$line"
done <$readPath > o
/bin/mv o $readPath

#rm ./public/.html 2>/dev/null

 #echo "${litem[@]}"&&exit

for path in ${litem[@]}
do

/bin/cat > ./public/${path[@]} <<- _EOF_
<html>
<!-- headtags -->
  <head>
  $headtags
  </head>
<title>Insert custom page title here</title>
<!-- end headtags -->
<body>

 <!-- navbar -->
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="./"><img src="./shared/img/jetpack.png" alt="She-Bang logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      ${nav0[@]} 
    </ul>
    <form class="form-inline ml-auto" style="margin-bottom: 0;">
      <input class="form-control mr-sm-2" type="search" placeholder="Search">
      <button class="btn btn-primary" type="submit" style="margin-right: 40px;">SEARCH</button>
      <a href="./login.html" class="btn btn-primary" role="button" aria-pressed="true">LOGIN/SIGN-UP</a>
    </form>
  </div>  
</nav>
 <!-- navbar -->

<!-- app title -->
    <div class="title-main">
        <h2 id="title-main">${path[@]%.*}</h2>
    </div>
<!-- app title -->

<!-- custom application page start -->
  <div id="content" align="center">
    <h3>Place your custom page content here</h3>
  </div>


<br>

<div class="container">
  <div class="row">
    <div class="col text-center">
      <a class="button white" href="https://www.patreon.com/join/nkrtech/checkout">SUPPORT</a>
    </div>
  </div>
</div>
<!-- custom application page end -->

<!-- Footer -->
  $footer
<!-- Footer -->
</body>
</html>
_EOF_
done
# Generate the index route
  ./scripts/home.sh

# Create Login/registration pages

# Login page
/bin/cat > ./public/login.html <<- _EOF_
<html>
<!-- headtags -->
  <head>
  $headtags
  </head>
<title>Insert custom page title here</title>
<!-- end headtags -->
<body>
 <!-- navbar -->
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="./"><img src="./shared/img/jetpack.png" alt="She-Bang logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      ${nav0[@]} 
    </ul>
    <form class="form-inline ml-auto" style="margin-bottom: 0; display: none;">
      <input class="form-control mr-sm-2" type="search" placeholder="Search">
      <button class="btn btn-primary" type="submit" style="margin-right: 40px;">Search</button>
      <a href="./login.html" class="btn btn-primary" role="button" aria-pressed="true">LOGIN/SIGN-UP</a>
    </form>
  </div>  
</nav>
 <!-- navbar -->

<!-- app title -->
    <div class="title-main">
        <h2 id="title-main">Login to Your Account</h2>
    </div>
<!-- app title -->

<!-- custom application page start -->
 $loginForm
<!-- custom application page end -->

<br>
<!-- My donate button -->
<div class="container">
  <div class="row">
    <div class="col text-center">
      <a class="button white" href="https://www.patreon.com/join/nkrtech/checkout">SUPPORT</a>
    </div>
  </div>
</div>
<!-- Donate button -->

<!-- Footer -->
  $footer
<!-- Footer -->

</body>
</html>
_EOF_

#======================================= Register page ============================0
/bin/cat > ./public/register.html <<- _EOF_
<html>
<!-- headtags -->
  <head>
  $headtags
  </head>
<title>Insert custom page title here</title>
<!-- end headtags -->
<body>

 <!-- navbar -->
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="./"><img src="./shared/img/jetpack.png" alt="She-Bang logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      ${nav0[@]} 
    </ul>
    <form class="form-inline ml-auto" style="margin-bottom: 0; display: none;">
      <input class="form-control mr-sm-2" type="search" placeholder="Search">
      <button class="btn btn-primary" type="submit" style="margin-right: 40px;">Search</button>
      <a href="./login.html" class="btn btn-primary" role="button" aria-pressed="true">Login/Sign-Up</a>
    </form>
  </div>  
</nav>
 <!-- navbar -->

<!-- app title -->
    <div class="title-main">
        <h2 id="title-main">Register for an Account</h2>
    </div>
<!-- app title -->

<!-- custom application page start -->
 $regForm
<!-- custom application page end -->

<br>
<!-- My donate button -->
<div class="container">
  <div class="row">
    <div class="col text-center">
      <a class="button white" href="https://www.patreon.com/join/nkrtech/checkout">SUPPORT</a>
    </div>
  </div>
</div>
<!-- Donate button -->

<!-- Footer -->
  $footer
<!-- Footer -->
</body>
</html>
_EOF_

# Password recovery route
/bin/cat > ./public/recover.html <<- _EOF_
<html>
<!-- headtags -->
  <head>
  $headtags
  </head>
<title>Insert custom page title here</title>
<!-- end headtags -->
<body>

 <!-- navbar -->
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="./"><img src="./shared/img/jetpack.png" alt="She-Bang logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      ${nav0[@]} 
    </ul>
    <form class="form-inline ml-auto" style="margin-bottom: 0; display: none;">
      <input class="form-control mr-sm-2" type="search" placeholder="Search">
      <button class="btn btn-primary" type="submit" style="margin-right: 40px;">Search</button>
      <a href="./login.html" class="btn btn-primary" role="button" aria-pressed="true">Login/Sign-Up</a>
    </form>
  </div>  
</nav>
 <!-- navbar -->

<!-- app title -->
    <div class="title-main">
        <h2 id="title-main">Recover Your Password</h2>
    </div>
<!-- app title -->

<!-- custom application page start -->
 $recPass
<!-- custom application page end -->

<br>
<!-- My donate button -->
<div class="container">
  <div class="row">
    <div class="col text-center">
      <a class="button white" href="https://www.patreon.com/join/nkrtech/checkout">SUPPORT</a>
    </div>
  </div>
</div>
<!-- Donate button -->

<!-- Footer -->
  $footer
<!-- Footer -->
</body>
</html>
_EOF_
# End password recovery route

exit_status=$?
if [ $exit_status -ge 1 ]; then
    echo -e "${RED}Error! Script exited with errors. Please check individual error messages to fix this${NC}"&&exit
else
  echo " "
  echo "Corresponding route files were created in the public directory"
  echo "-------------------------------------------------------------"
  echo $'1) Ensure all dependencies are installed.\n2) Run the command *npm run install* in your project directory.\n3) Spin the server by running *npm run dev*'
fi
exit $exit_status

routing
) > $router
#======================================= End of routes ====================================0

#======================================= Writing global variables =========================0
#sleep 2s
global_vars=./$prName/variables/global.sh
(
cat << 'globals'
#!/bin/bash

APIPath="https://api.exchangeratesapi.io/latest?base=EUR"
delay=5s

globals
) > $global_vars
#======================================= End of global variables ============================0


#======================================== She-Bang logo =====================================0
git clone https://rotichkibet@bitbucket.org/rotichkibet/img.git ./$prName/public/shared/img
#======================================= She-Bang logo  =====================================0 

#====================================== writing external styles =============================0
main_css=./$prName/public/shared/main.css
(
cat << 'mains'
body {
  background-color: white;
}
.title-main {
  color: yellow;
  text-align: center;
  background-color: black;
}
#title-main{
  color: yellow;
}

#special-sponsor{
text-align: center;
padding-bottom: 30px;
}

#headings{
text-align: center;
padding: 20px 100px 20px;
}

.feature{
padding: 0.5rem;
}

#framework{
text-align: center;
padding: 20px;
}

.card {
  margin: auto;
  padding-left: 3.5rem;
  padding-right: 3.5rem;
  width: 100%;
  height: 100%;
  align-content: center;
}

.desc {
  text-align: center;
}
#closeBtn {
  padding-right: 3.5rem;
  display: inline;
  text-align: right;
}
.thread {
  background-color: black;
  color: yellow;
}
.btn-primary {
  background-color: black;
  color: yellow;
  border-color: black;
}
.in-brief{
  margin-left: 0;
}
.table table-bordered td {
      text-align: center; 
    vertical-align: middle;
}
.page-footer font-small special-color-dark pt-4{
  background-color: whitesmoke;
  color:  #4885ed;
}

a#nav-links {
  color: aliceblue;
}
input {
  margin-bottom: 0;
}
.footer-copyright.text-center.py-3 {
  color: #4885ed;
  background-color: #343a40;
  position: fixed;
  bottom: 0;
  width: 100%;
}
label{
  font-weight: bold;
}

.features {
    display: flex;
    padding: 20px;
    margin-left: 10%;
    margin-right: 10%;
}

.h2 {
  width: 100%;
  height: 100%;
  background-color: #E0FFF4;
}

p{
 word-spacing: 0.05rem;
}

#jetpack{
text-align: center;
}

@media screen and (max-width: 900px) {
  input {
    margin-bottom: 20px;
  }
  .footer-copyright.text-center.py-3 {
    color: #4885ed;
    background-color: #343a40;
    position: fixed;
    bottom: 0;
    width: 100%;
  }
  .card{
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    margin-left: 1.5rem;
  }
.features {
   -webkit-box-orient: vertical;
   -webkit-box-direction: normal;
   flex-direction: column;
   padding-left: 0.1rem;
  }

}
mains
) > $main_css
#====================================== end of external styles ==================================0

echo "========== ... writing application directories =============================="
#sleep 1s
applic_file=./$prName/$appName.sh
(
cat << 'app_file'
#!/usr/bin/env bash

source ./templates/nav.sh
source ./templates/footer.sh
source ./variables/global.sh
source ./templates/headtags.sh

demo_filePath="./public/api-fetch.html"

while :; do
  
  # Declare all local variables here
  pageTitle="DEMO Latest Rates Against $base as of $(date +"%x %r %Z")"
#  entryDate=$(curl -s $APIPath  | jq '.date') 
#  entryTime=$(curl -s $APIPath  | jq '.time_last_updated') 
  GBP=$(curl -s $APIPath  | jq -r '.rates.GBP') 
  USD=$(curl -s $APIPath  | jq -r '.rates.USD') 
  AUD=$(curl -s $APIPath  | jq -r '.rates.AUD') 
  SGD=$(curl -s $APIPath  | jq -r '.rates.SGD') 
  CHF=$(curl -s $APIPath  | jq -r '.rates.CHF')
  BGN=$(curl -s $APIPath  | jq -r '.rates.BGN') 
  SEK=$(curl -s $APIPath  | jq -r '.rates.SEK') 
  CAD=$(curl -s $APIPath  | jq -r '.rates.CAD') 
  NZD=$(curl -s $APIPath  | jq -r '.rates.NZD') 
  ILS=$(curl -s $APIPath  | jq -r '.rates.ILS')
  JPY=$(curl -s $APIPath  | jq -r '.rates.JPY') 
  base=$(curl -s $APIPath  | jq -r '.base') 

cat > $demo_filePath <<- _EOF_
<!DOCTYPE html>
<html lang="en">
  <!-- headtags -->
   <title> An API end-point data acquisition </title>
   <meta http-equiv="refresh" content="5" >
   <head>
    $headtags
   </head>
  <!-- end headtags -->
  <body>

      <!-- navbar -->
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="./"><img src="./shared/img/jetpack.png" alt="She-Bang logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      ${nav0[@]} 
    </ul>
    <form class="form-inline ml-auto" style="margin-bottom: 0;">
      <input class="form-control mr-sm-2" id="the-form" type="search" placeholder="Search">
      <button class="btn btn-primary" type="submit" style="margin-right: 40px;">SEARCH</button>
      <a href="./login.html" class="btn btn-primary" role="button" aria-pressed="true">LOGIN/SIGN-UP</a>
    </form>
  </div>  
</nav>
      <!-- navbar -->

      <!-- app title -->
      <div class="title-main">
        <h2 id="title-main">$pageTitle</h2>
      </div>
      <!-- app title -->

    <!-- custom application start -->
      <ul class="nav justify-content-end">
        <li class="endpoint">
          <a class="nav-link" href="$APIPath"> ECB API Endpoint</a>
        </li>
      </ul>
      <div class="card">
  <table class="table table-bordered">
    <thead class="thread">
      <tr>
        <th>Currency Name</th>
        <th>Currency Symbol</th>
        <th>Exchange Rate</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <strong><td>Great Britain Pound</td></strong>
        <td>GBP</td>
        <td>$GBP</td>
      </tr>        
        <td>US Dollar</td>
        <td>USD</td>
        <td>$USD</td>
      </tr>       
        <td>Australian Dollar</td>
        <td>AUD</td>
        <td>$AUD</td>
      </tr>         
        <td>Swiss Franc.</td>
        <td>CHF</td>
        <td>$CHF</td>
      </tr>
        <td>Swedish Krona</td>
        <td>SEK</td>
        <td>$SEK</td>
      </tr>
        <td>Canadian Dollar</td>
        <td>CAD</td>
        <td>$CAD</td>
      </tr>
        <td>New Zealand Dollar</td>
        <td>NZD</td>
        <td>$NZD</td>
      </tr>
        <td>Israel New Shekel</td>
        <td>ILS</td>
        <td>$ILS</td>
      </tr> 
        <td>Japanese Yen</td>
        <td>JPY</td>
        <td>$JPY</td>
      </tr>
    </tbody>
  </table>
</div>

<br>
<div class="container">
  <div class="row">
    <div class="col text-center">
       <a class="button white" href="https://www.patreon.com/join/nkrtech/checkout">SUPPORT</a>
    </div>
  </div>
</div>
    <!-- custom application end -->      


    <!-- Footer -->
      $footer
    <!-- Footer -->
  </body>
</html>
_EOF_
done
sleep $delay
rm $demo_filePath
app_file
) > $applic_file
#======================================= writing the application =============================0
#====================================== writing home page ================================0
home_file=./$prName/scripts/home.sh
(
cat << 'home_file'
#!/bin/bash

source ./templates/nav.sh 2>/dev/null
source ./templates/footer.sh
source ./templates/headtags.sh

index_filepath=./$prName/public/index.html

/bin/cat > $index_filepath <<- _EOF_ 
<!DOCTYPE html>
 <html>
   <!-- headtags -->
    <head>
      <title>JetPack Framework </title>
      $headtags
    </head>
  <!-- end headtags -->
  <body>
 <!-- navbar -->
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="./"><img src="./shared/img/jetpack.png" alt="She-Bang logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      ${nav0[@]} 
    </ul>
    <form class="form-inline ml-auto" style="margin-bottom: 0;">
      <input class="form-control mr-sm-2" type="search" placeholder="Search">
      <button class="btn btn-primary" type="submit" style="margin-right: 40px;">SEARCH</button>
      <a href="./login.html" class="btn btn-primary" role="button" aria-pressed="true">LOGIN/SIGN-UP</a>
    </form>
  </div>  
</nav>
 <!-- navbar -->



<!-- custom application start -->

<div id="hero">
  <div class="inner">
    <div class="right">
      <h1 id="framework">
        JetPack| A Linux Web Framework<br>
      </h1>
      <p id="headings">
        <a class="button white" href="https://jet-pack.netlify.com/">GET STARTED</a>
        <a class="button gray has-icon" href="https://github.com/moinonin/jetpack-framework">
          <img src="./shared/img/github.png">
          GITHUB</a>
      </p>
    </div>
  </div>
</div>

<div id="special-sponsor">
  <h3>Official Sponsor</h3>
  
  <a href="https://inzpirogaming.com/" target="_blank">
    <img src="./shared/img/inzpiro.png" style="width:160px" alt="inzpirogaming Logo">  </a>
    <br>
    <span>Our software creates yours. Check our </span> <a href="./shared/img/guide.pdf">guide</a> for reference.
</div>

    <h1 id="jetpack">
        <!--<img src="./shared/img/jetpack32.png">-->
          WHY JETPACK?
    </h1>

<div class="features">
    <div class="feature">
        <h2>Easy-Peasy</h2> 
            <p class="content">If you already know some HTML, CSS, and JavaScript, combined with a styling tool such as bootstrap, this tool will get you started in no time!
            </p>
    </div>
    <div class="feature">
        <h2>Incredibly fast</h2> 
            <p class="content">An easy developmental framework that scales down your work while speeding up your actual backend coding. Leave the client side to us!
            </p></div>
    <div class="feature">
        <h2>High performance</h2> 
            <p class="content">The framework takes up only less than 0.3 mB installed disk capacity. It is therefore cheap computationally, with minimal need for   configuration.
            </p>
    </div>
</div>
<br>

<div class="container">
  <div class="row">
    <div class="col text-center">
      <a class="button white" href="https://www.patreon.com/join/nkrtech/checkout">SUPPORT</a>
    </div>
  </div>
</div>
<!-- custom application end -->      

  <!-- Footer Elements -->
      $footer
  <!-- Footer Elements -->
  </body>
</html>
_EOF_

home_file
) > $home_file
#====================================== end homepage write-up ================================0
#====================================== package.json =========================================0
json=./$prName/package.json
(
cat <<  json_script
{
  "name": "jetpack",
  "version": "2.0.1",
  "description": "To start development, run 'npm run dev' to generate routes, and spin the server in one go",
  "main": "index.js",
  "scripts": {
    "app": "./$appName.sh",
    "serve": "http-server",
    "home": "./scripts/home.sh",
    "jetpack": "npm run serve & npm run app",
    "clone-server": "./scripts/apigen.sh",
    "halt": "sudo killall app.sh && sudo killall bash",
    "litem": "./scripts/navgen.sh",
    "rot": "./scripts/gen.sh",
    "routgen": "npm run litem && npm run rot",
    "dev": "npm run litem && npm run rot && npm run jetpack",
    "build": "./.build.sh"
  },
  "devDependencies": {

  },
  "keywords": [
    "jetpack",
    "Exchange",
    "Shell",
    "Script"
  ],
  "author": "Nicolus Rotich",
  "license": "UNLICENSED",
  "dependencies": {
    "http-server": "^0.11.1"
  },
  "repository": {
    "type": "git",
    "url": "https://github.com/moinonin/shebang-framework.git"
  }
}


json_script
) > $json

#============================================ end of package.json ============================0

#======================================= Writing guide ============================0
#sleep 2s
guide=./$prName/README.md
(
cat << 'guides'
# ![](http://imgur.com/kqwYh3Pl.png) __GETTING STARTED__ ![](http://imgur.com/rRGC6N8l.png)  

# Getting Started

## Introducing Jetpack Framework
Jetpack is a foundational web framework leveraging all bash shell and Unix capabilities to give you a state-of-the-art static site generator, and API endpoint fetcher. Here we are referring to portability of bash, richness in regular expression (regex) libraries, accessibility to the filing systems, and the light-weightiness of bash shell. The application uses heredocuments to create script templates for you to start out with. All these enable Jetpack to come up with two main purposes that most modern developers are intrigued about:
### Static Site Generation
Sometimes you just want to create a site with no serverside embedded data. For instance documentation website, landing pages, portfolios, and what have you. These are the kind of tasks that shouldn't waste any of your money, as we know time = money :) She-Bang will harness its magic to create a site for you, literally within seconds.
### API data fetcher
The second utility She-Bang comes with is the ability to fetch data from API endpoints. Our tool comes with a DEMO page named "api-fetch.html", which enables getting of data from any API url, or locally available *.json file. It's real-time capability is enabled by an infinite loop that is used to read the information from the server, write them to an html file, and discard after a set loop (sleep) time. Upon expiry of the sleep time, the file is deleted and a fresh one is created with newer information. Users should be aware of the rate at which information is fetched from the server, set sleep time with a variable "delay" under the scripts directory 1-5 seconds was used for the trials and worked best for that specific API endpoint provided by the European Central Bank (ECB). In your case, however, check with the server administrator to agree on the delay time. This is IMPORTANT! as when the delay is set too short, the application will force the server to throttle the rate in which data is distributed to your local machine.
## Requirements
Jetpack framework is currently available only for 64 bit linux (all distros) and Windows 64-bit users. Depending on the interest, the team behind it will soon roll out MacOs versions. Besides, the package uses node package management, and thus you need nodejs installed (and therefore npm).
## Downloads and Installations  

### Windows 64-bit Users:  
#### Installation  
[Windows 64-bit](https://github.com/moinonin/shebang-framework/raw/master/shebang_1.01-1_win64.zip)  
For windows users, jetpack can be cloned or downloaded directly from [github repository](https://github.com/moinonin/shebang-framework). The rest of the installation follows extracting the zip file to your preferred location, opening the content folders, and double-clicking the executable file. That will install the program for everyday use. You may optionally create a desktop shortcut just as you do with many of your applications.  

### Linux X86-64 Users:
[Linux X86-64](https://github.com/moinonin/shebang-framework/raw/master/shebang_1.01-1_amd64.deb)
#### Installation
Upon cloning or downloading the application from our [github repository](https://github.com/moinonin/shebang-framework), open your favorite terminal application, navigate to the source directory (where it is downloaded to) and install it by running the following commands:
```
$ sudo apt-get install ./shebang_1.01-1_amd64.deb
```
Don't worry about the location of installed files, it is automatically set to go to usr/bin where all other applications are installed. While installing and once it is installed, you should see an output like this:  
================== start =========>  
Reading package lists... Done  
Building dependency tree  
Reading state information... Done  
Note, selecting 'shebang' instead of './shebang_1.01-1_amd64.deb'  
The following NEW packages will be installed:  
shebang  
0 upgraded, 1 newly installed, 0 to remove and 1 not upgraded.  
After this operation, 272 kB of additional disk space will be used.  
Get:1 /home/rotich/Desktop/portfolio/projects/shell/bash/packaged/she-bang/  
shebang_1.01-1_amd64.deb shebang amd64 1.01-1 [30.8 kB]  
Selecting previously unselected package shebang.  
(Reading database ... 269205 files and directories currently installed.)  
Preparing to unpack .../shebang_1.01-1_amd64.deb ...  
Unpacking shebang (1.01-1) ...  
Processing triggers for menu (2.1.47+b1) ...  
Setting up shebang (1.01-1) ...  
Processing triggers for menu (2.1.47+b1) ...  
================== end =========>  
## Jetpack Usage
Navigate to the area of your projects, say desktop, create a directory you would like to use for your projects say you want to call it "my-project", you will then do this:
```
$ mkdir my-project
```
Change directory to the project as follows:
```
$ cd my-project
```
Next, run the following command:
```
$ shebang
```
Our dedicated command line interface (CLI) will begin a dialog immediately, and you will just need to follow the directions on the CLI.
```
$ Enter project name (shebang ?):
```
Enter the project folder you wish to create - (if you are lazy like me, press enter with no input) the default is the preceeding directory -- in that case "my-project". However, let's keep things in order and call the actual application "my-website":
```
$ Enter application entry point (default 'app.sh'?):
```
Enter the application name, the default one is "app.sh" if you don't enter anything. However, I choose to call my main application script "index.sh", the following text will follow if all goes well.  
#=================== start ====================================#  
===== wait as we prepare your bang project =========  
Cloning into './my-website/public/shared/img'...  
remote: Counting objects: 37, done.  
remote: Compressing objects: 100% (37/37), done.  
remote: Total 37 (delta 11), reused 0 (delta 0)  
Unpacking objects: 100% (37/37), done.  
===== ... writing application directories ===========  
all set! cd to (my-website) and see if it worked  
Ensure all dependencies are installed.  
Run the command *npm run install* for the dependencies.  
Initiate the project by running *npm run dev*.  
#===================end ======================================#  
Do as the application says: navigate to the project directory you just created. Install the dependencies by doing the following:
```
$ cd my-website
```

```
$ sudo npm install
```
```
$ npm run dev
```
This is the magic command, it creates the routes, template scripts, and even create our own first page (index.html) to direct you to our documentation and guides. On running above command, you will be prompted to enter navigation bar items:
```
$ Enter navbar items (#word.html separated by spaces):
```
For the purposes of this example, to create two routes, I will enter "quick brown-fox.html". Note, you can choose to add file extensions to the words or live them the way they are as you can see, I left "quick" without extension, but added .html to "brown-fox". The next step is to spin the development server and the demo app by running:
```
$ npm run demo
```
This is your usual command used to trigger the localhost server. By default, Jetpack uses port 8080. If you get a server error, make sure to install "http-server" by typing.
```
$ npm install -g http-server
```
You should see the following output.  
#=================== start ====================================#  
> she-bang@1.0.0 dev /home/rotich/Desktop/shebang/my-website  
> npm run serve & npm run app  
> she-bang@1.0.0 serve /home/rotich/Desktop/shebang/my-website  
> http-server  
> she-bang@1.0.0 app /home/rotich/Desktop/shebang/my-website  
> ./index.sh  

Starting up http-server, serving ./public  
Available on:  
http://127.0.0.1:8080  
http://192.168.1.101:8080  
Hit CTRL-C to stop the server  
#===================end ======================================#  
Navigate to your favorite browser and type:  

```
http://localhost:8080
```
or:
```
http://127.0.0.1:8080
```
After this, you are ready to carry on the development. You should see She-Bang home page below:
![](http://imgur.com/XFaIAc4l.png)
Familiarize yourself with the project directories created and where necessary consult this guide.
## Understanding files & directories
In the next sections, we will explain more about the files and directories She-Bang needs in order to run flawlessly. There are a total of seven directories (including the top one hosting the website -- named here "my-project"), and 18 files created by She-Bang. See the following file and directories tree:
```
├── index.sh
├── package.json
├── public
│ ├── api-fetch.html
│ ├── brown-fox.html
│ ├── index.html
│ ├── quick.html
│ └── shared
│ ├── bootstrap.css
│ ├── img
│ │ ├── inzpiro.png
│ │ └── jetpack.png
│ ├── index.css
│ └── main.css
├── scripts
│ ├── gen.sh
│ ├── home.sh
│ └── navgen.sh
├── templates
│ ├── footer.sh
│ ├── headtags.sh
│ └── nav.sh
└── variables
└── global.sh
6 directories, 18 files
```
## Directories
### public
This is the directory hosting html files, or routes and assets such as cascaded style sheets (css) and image files.
### shared
This directory contains the assets, images, and css, and the folder is itself hosted under the public directory.
### img
The folder contains our logo and those of our official sponsor [inzpiro gaming](https://inzpirogaming.com/). You will be able to leverage and utilize this directory to your liking, provided that your route files will be able to get access to the assets referenced.
### scripts
As the directory name suggests, it contains Unix shell scripts that are run through node package manager for She-Bang to work efficiently. The use of bash in itself is the novelty behind she-bang, as they are fast in execution, and are portable, meaning they can be run on either Linux or windows, or mac OS platforms. Windows 10 particularly these days support Linux-like environment where She-Bang can be executed. We have not tested this service though, but we hope to get it going soon. In summary, "navgen.sh" generates the navigation bar items and converts them into a variable "nav0", which is then rendered in the html files where the navigation bar list items should appear. The homepage aka index.html is actually created and rendered by the script "home.sh", and through looping, the script "gen.sh" writes all the rest of the routes that are presented here as plain templates where you can add your page title and page contents.
### templates
There are certain repetitive tasks that shouldn't bother you as a developer. These are things like the head components of an html file, the footer including the copyright information, and the navigation bar items. All these are written by our scripts stored in the templates directory. Accordingly, this folder contains three files, nav.sh -- for the navigation bar items, footer.sh as the name suggests, and headtags.sh.
### Other important files
#### index.sh
This is the main application file that creates the API DEMO fetch, it is basically an infinite loop that continuously creates and destroys the data imported from the server, and renders the new results each time to the client or the front-end.
#### package.json
Is the node package management file containing all the project details, scripts, dependencies, etc. You can read more on this under the node package management official site.
#### global.sh
This is a file that stores global variables that can be called in any file when required. In this case, for instance, one such variable was "APIPath", which as you might have guessed, represents the url in which the API is hosted. The second variable was the "delay", which is time set in seconds at which the application should read the server. This is important as if many hits are sent to the server, it will sense possible hacking, and will immediately throttle the data streaming behavior. It is therefore IMPORTANT! to set the time delay appropriately. If necessary, contact the server administrator.
## Customizing the application
Now that we have learned how to install the application and examined the contents of each directory as well as their uses, we would naturally want to tweak these templates and variables to use them for our new site. You will thus start by heading to [bootstrap](https://getbootstrap.com), [bulma](https://bulma.io) or any styling content delivery network (CDN) of your choice, and get your templates. Replace the scripts head tags, and footer (including the social buttons and copyright), nav script will be generated automatically thanks to the power of She-Bang. You will notice that the above scripts are re-written into variables that will be rendered in the home and other subsequent routes. It is therefore important to ensure that you know what you are doing, otherwise you will have to start over and over -- which is not a bad idea if you want to learn to be proficient in this software.

Now, head to scripts folder where you will meet home, navgen, and gen shell scripts. As you might have guessed, the home script generates the home page, aka index.html. The list items of the navigation bar are created by navgen, while gen shell script creates all the other routes. Once you have modified all the scripts right, you are now ready to generate your site by running the following commands subsequently:
```
$ npm run rot # to generate the routes based on the navgen script
```
```
$ npm run demo # to spin the development server and any other apps  
```
A continuous iteration of these steps will definitely result in a great website for your portfolio, landing page, or documentation. You will also gain cutting-edge skills utilizing this software and hopefully be one of the best consultants out there.
## Conclusion
If you have read this guide up to this point, you should be ready to hit the road with She-Bang static site generator and API fetcher. It is my sincere hope that you will find as much joy in utilizing this tool, as I did writing its code.

![](http://imgur.com/pD3Qr5Cl.png)  
__Software Author__: Nicolus K. Rotich  
__Contact__: [Linkedin](https://linkedin.com/in/rotichtheengineer/)  

---
##### <Footer></Footer>
---
guides
) > $guide
#======================================= End of guide ============================0

#======================================== API clone ==============================0
server_clone=./$prName/scripts/apigen.sh
(
cat << 'apiclones'
#!/bin/bash

source ./variables/global.sh

# set API path to read from 
# Important! remember to enter authentication key if available

api=$(curl -s $APIPath  | jq '.')

filePath="./public/shared/api.json"

cat > $filePath <<- _EOF_

        $api

_EOF_ 

apiclones
) > $server_clone
#======================================== End API clone ==========================0


#======================================== Login Form ==================0
logIn=./$prName/templates/login.sh
(
cat << 'log_in'
#!/bin/bash

read -r -d '' loginForm <<- EOM
 <div class="container">
  <div class="row justify-content-md-center">
    <div class="jubmbotron text-left">
      <h3 class="display-6"></h3>
        <div class="card"> 
   <form>
            <div class="form-group">
              <label for="exampleInputEmail1"><strong>Email address</strong></label>
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1"><strong>Password</strong></label>
              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">Remember me</label>
            </div>
            <button type="submit" class="btn btn-primary">Login</button>
              <p style="margin-left: 0; font-size: 12">Don't have an accout?<a href="./register.html" > Register </a></p>
              <p style="margin-left: 0; font-size: 12">Forgot password?<a href="./recover.html" > Recover</a></p>  
  </form>
        </div>
    </div>
  </div>
</div>
EOM

log_in
) > $logIn
#======================================== End Login ==================0


#======================================== registration Form ==========0
registrationForm=./$prName/templates/regist.sh
(
cat << 'reg'
#!/bin/bash

read -r -d '' regForm <<- EOM
 <div class="container">
  <div class="row justify-content-md-center">
    <div class="jubmbotron text-left">
      <h3 class="display-6"></h3>
        <div class="card">
<form>
          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Password</label>
                <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
            </div>
          </div>
            <div class="form-group">
              <label for="inputAddress">Address</label>
              <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
            </div>
          <div class="form-group">
            <label for="inputAddress2">Address 2</label>
            <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCity">City</label>
                <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="form-group col-md-4">
                <label for="inputState">State</label>
                <select id="inputState" class="form-control">
                <option selected>Choose...</option>
                <option>...</option></select>
            </div>
          <div class="form-group col-md-2">
              <label for="inputZip">Zip</label>
              <input type="text" class="form-control" id="inputZip">
          </div>
        </div>
        <div class="form-group">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label" style="font-size: 12" for="gridCheck">I agree to the terms of service</label>
        </div>
        </div>
            <button type="submit" class="btn btn-primary">Register</button><span style="float: right" ><a href="./login.html" ><small>Login Instead</small></a></span>
</form>
        </div>
    </div>
  </div>
</div>
EOM

reg
) > $registrationForm
#======================================== End registration script ==================0

#======================================== recover pass script ======================0
rec=./$prName/templates/recover.sh
(
cat << 'rec_pass'
#!/bin/bash

read -r -d '' recPass <<- EOM
 <div class="container">
  <div class="row justify-content-md-center">
    <div class="jubmbotron text-left">
      <h3 class="display-6"></h3>
        <div class="card">
          <hr>
                        <div class="text-center">
                          <h3><i class="fa fa-lock fa-4x"></i></h3>
                          <h2 class="text-center">Forgot Password?</h2>
                          <p>You can reset your password here.</p>
                            <div class="panel-body">
                              
                              <form class="form">
                                <fieldset>
                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                      
                                      <input id="emailInput" placeholder="Email address" class="form-control" type="email" oninvalid="setCustomValidity('Please enter a valid email address!')" onchange="try{setCustomValidity('')}catch(e){}" required="">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <input class="btn btn-lg btn-primary btn-block" value="Send My Password" type="submit">
                                  </div>
                                </fieldset>
                              </form>
                              
                            </div>
                        </div>
        </div>
    </div>
  </div>
</div>
EOM

rec_pass
) > $rec

#======================================== End recover pass script ================0
#======================================= Build Start =========================0
#sleep 2s
build_script=./$prName/.build.sh
(
cat << 'builds'
#!/bin/bash

mkdir ./.jetpack/dist

cp -r ./public/* ./.jetpack/dist/


#final test for exit code
exit_status=$?
if [ $exit_status -ge 1 ]; then
    echo  -e "\e[31mError: Unsuccesful build attempt! Please check individual error messages to fix this\e[0m"
else
  echo -e $'\e[32mBuild succesful.\nFind your built files at /.jetpack/dist\nThe project is ready for deployment\e[0m'
fi
exit $exit_status

builds
) > $build_script
#======================================= Build End============================0
#======================================= Writing Service File =========================0
#sleep 2s
#service_script=./$prName/$appName.service
#(
#cat << services
#[Unit]
#Description=$appName service
#After=network.target

#[Service]
#Type=simple

#ExecStart=/usr/bin/env bash /home/nr/Desktop/my-site/$appName.sh

#[Install]
#WantedBy=default.target

#services
#) > $service_script
#======================================= End of Service File ============================0




#======================================== allow file permisions ==================0
chmod +x ./$prName/$appName.sh
chmod +x ./$prName/scripts/*.sh
chmod +x ./$prName/templates/*.sh
chmod +x ./$prName/variables/*.sh
chmod +x ./$prName/.build.sh
#chmod +x ./$prName/$appName.service
#sudo cp ./$prName/$appName.service ~/.config/systemd/user
#======================================== end permisions =========================0

#final test for exit code
exit_status=$?
if [ $exit_status -ge 1 ]; then
    echo -e "\e[31mSomething went wrong! Please check individual error messages to fix this\e[0m"
else
  echo -e "\e[32mall set! cd to $prName and see if it worked\e[0m"
  echo -e $'\e[32mEnsure all dependencies are installed.\nRun the command *npm run install* for the dependencies.\nInitiate the project by running *npm run rot*\e[0m'
fi
exit $exit_status

else
echo -e "\e[31m================= Your JetPack version has expired =======================\e[0m"
echo -e "\e[31mContact your software vendor for renewal\nAdmin: Nicolus Rotich\nEmail: nrotich@nkrtech.com\nThank you for using JetPack-Framework\e[0m"
echo -e "\e[31m================= Your JetPack version has expired =======================\e[0m"
fi